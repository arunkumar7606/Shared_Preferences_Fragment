package com.arun.aashu.sharedprefassignment1.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.arun.aashu.sharedprefassignment1.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoginPageFragment extends Fragment {

    private EditText edName, edPassword;
    private TextView tvName, tvPassword;
    private CheckBox checkBox;
    private Button loginButton, retrieveButton, clearButton;
    private View view;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    private Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    public LoginPageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login_page, container, false);

        edName = view.findViewById(R.id.edName);
        edPassword = view.findViewById(R.id.edPassword);

        tvName = view.findViewById(R.id.textName);
        tvPassword = view.findViewById(R.id.textPassword);

        checkBox = view.findViewById(R.id.checkBox);

        loginButton = view.findViewById(R.id.loginButton);
        retrieveButton = view.findViewById(R.id.retrieveButton);
        clearButton = view.findViewById(R.id.clearButton);


        sharedPreferences = context.getSharedPreferences("jadu", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String name = edName.getText().toString();
                String password = edPassword.getText().toString();

                if (name.equals("")) {

                    edName.setError("Fill Name Please");


                } else if (password.equals("")) {
                    edPassword.setError("Fill Password");
                } else {

                    editor.putString("name_key", name);
                    editor.putString("password_key", password);
                    editor.commit();

                    HomeFragment welcomeFragment = new HomeFragment();
                    Bundle bundle = new Bundle();

                    bundle.putString("name_key", name);
                    welcomeFragment.setArguments(bundle);

                    FragmentManager fm = getFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    ft.replace(R.id.main_page, welcomeFragment);
                    ft.addToBackStack(null);
                    ft.commit();

                    edName.setText("");
                    edPassword.setText("");
                }
            }
        });

        retrieveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String name = sharedPreferences.getString("name_key", null);
                String password = sharedPreferences.getString("password_key", null);

                if (name != null && password != null) {
                    tvName.setText(name);
                    tvPassword.setText(password);

                } else {
                    tvName.setText("No Data Available");
                    tvPassword.setText("No Data Available");
                }
            }
        });

        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                editor.clear();
                editor.commit();
            }
        });

        // Inflate the layout for this fragment
        return view;
    }


}


