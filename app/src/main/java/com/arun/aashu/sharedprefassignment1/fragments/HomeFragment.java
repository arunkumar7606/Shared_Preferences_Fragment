package com.arun.aashu.sharedprefassignment1.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.arun.aashu.sharedprefassignment1.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private TextView textView_PrefData;
    private Button btn_LogOut,btn_showData;
    private Context context;
    private View view;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private String stName,stPassword;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context=context;
    }

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view= inflater.inflate(R.layout.fragment_welcome, container, false);

        textView_PrefData=view.findViewById(R.id.textView_PrefData);
        btn_showData=view.findViewById(R.id.btn_showData);
       btn_LogOut=view.findViewById(R.id.btn_LogOut);


       sharedPreferences=context.getSharedPreferences("jadu",Context.MODE_PRIVATE);
       editor=sharedPreferences.edit();

        btn_showData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                stName= sharedPreferences.getString("name_key",null);
                stPassword= sharedPreferences.getString("password_key",null);

                if (stName!=null && stPassword!=null){
                    textView_PrefData.setText(stName+" " +stPassword);
                }
                else{
                    textView_PrefData.setText("No Data Available");
                }
            }
        });



       btn_LogOut.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

               editor.clear();
               editor.commit();

           }
       });

        // Inflate the layout for this fragment
        return view;
    }

}
