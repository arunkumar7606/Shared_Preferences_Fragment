package com.arun.aashu.sharedprefassignment1;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.arun.aashu.sharedprefassignment1.fragments.LoginPageFragment;


public class MainActivity extends FragmentActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fm=getSupportFragmentManager();
        FragmentTransaction ft=fm.beginTransaction();
        ft.add(R.id.main_page,new LoginPageFragment());
        ft.commit();

    }

    @Override
    public void onBackPressed() {

        final Dialog dialog=new Dialog(this);
        dialog.setContentView(R.layout.custom_dialog);

        dialog.setCancelable(true);
        dialog.show();


        dialog.findViewById(R.id.yesBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity.super.onBackPressed();

            }
        });

        dialog.findViewById(R.id.NoBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(MainActivity.this, "Thanks Thanks Thanks", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });





    }
}
