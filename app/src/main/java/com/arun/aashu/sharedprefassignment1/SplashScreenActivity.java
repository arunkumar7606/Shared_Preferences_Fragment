package com.arun.aashu.sharedprefassignment1;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class SplashScreenActivity extends AppCompatActivity {

    private TextView textView_splash;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private String stPassword,stName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        textView_splash = findViewById(R.id.textView_splash);

        sharedPreferences = getSharedPreferences("jadu", MODE_PRIVATE);
        editor=sharedPreferences.edit();

        stName=sharedPreferences.getString("name_key",null);
        stPassword=sharedPreferences.getString("password_key",null);

        if (stName!=null && stPassword!=null){

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(SplashScreenActivity.this, HomePage.class));
                }
            }, 4000);
        }
        else if (stName==null && stPassword==null){

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
                }
            }, 4000);

        }




    }
}
