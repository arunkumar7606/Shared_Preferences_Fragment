package com.arun.aashu.sharedprefassignment1.loginRegis;

import android.content.Context;
import android.content.SharedPreferences;

public class ShredPref {
    final static String FileName="pref_data";

    public static String readSharedToken(Context context, String access_token, String access_token_Value){

        SharedPreferences sharedPreferences=context.getSharedPreferences(FileName, Context.MODE_PRIVATE);
        return sharedPreferences.getString(access_token,access_token_Value);

    }

    public static void savedSharedToken(Context context, String access_token , String access_token_Value){

        SharedPreferences sharedPreferences=context.getSharedPreferences(FileName, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString(access_token,access_token_Value);
        editor.commit();
    }



    public static void clearSharedToken(Context context, String access_Token, String access_token_value){

        SharedPreferences sharedPreferences=context.getSharedPreferences(FileName, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.clear();
        editor.commit();

    }
}
