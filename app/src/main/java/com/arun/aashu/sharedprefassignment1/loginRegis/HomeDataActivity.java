package com.arun.aashu.sharedprefassignment1.loginRegis;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.arun.aashu.sharedprefassignment1.R;

public class HomeDataActivity extends AppCompatActivity {
    private TextView textView_yourData;
    private Button btn_show,btn_LoggingOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_data);

        textView_yourData=findViewById(R.id.textView_yourData);
        btn_show=findViewById(R.id.btn_show);
        btn_LoggingOut=findViewById(R.id.btn_LoggingOut);

        btn_show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String token=ShredPref.readSharedToken(HomeDataActivity.this,"MyToken",null);
if (token!=null){
    textView_yourData.setText(token);

}else{
    textView_yourData.setText("No Data Available");
}

            }
        });


        btn_LoggingOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShredPref.clearSharedToken(HomeDataActivity.this,"MyToken",null);
            }
        });

    }
}
