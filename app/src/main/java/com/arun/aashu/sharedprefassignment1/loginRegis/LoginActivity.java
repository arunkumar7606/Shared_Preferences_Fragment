package com.arun.aashu.sharedprefassignment1.loginRegis;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.arun.aashu.sharedprefassignment1.R;

public class LoginActivity extends AppCompatActivity {

    private EditText login_username;
    private Button btn_logIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login2);

        login_username=findViewById(R.id.login_username);
        btn_logIn=findViewById(R.id.btn_logIn);


        btn_logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

    ShredPref.savedSharedToken(LoginActivity.this,"MyToken",login_username.getText().toString());
                startActivity(new Intent(LoginActivity.this,HomeDataActivity.class));
            }
        });
    }
}
